
public class Ausgabeformatierung {

	public static void main(String[] args) {
		
		String s = "*";
		
		// Aufgabe 1
		
		System.out.print("\"Das ist ein Beispielsatz\" \n");
		System.out.print("Ein Beispielsatz ist das. \n");
		System.out.print("// Der Unterschied zwischen print und println ist, dass println einen Zeilenumbruch verursacht \n");

		//Aufgabe 2
		
		System.out.printf( "%20s\n", s );
		System.out.printf( "%21s\n", s + s + s );
		System.out.printf( "%22s\n", s + s + s + s + s);
		System.out.printf( "%23s\n", s + s + s + s + s + s + s);
		System.out.printf( "%24s\n", s + s + s + s + s + s + s + s + s );
		System.out.printf( "%25s\n", s + s + s + s + s + s + s + s + s + s + s);
		System.out.printf( "%26s\n", s + s + s + s + s + s + s + s + s + s + s + s + s);
		System.out.printf( "%27s\n", s + s + s + s + s + s + s + s + s + s + s + s + s + s + s);
		System.out.printf( "%21s\n", s + s + s );
		System.out.printf( "%21s\n", s + s + s ); 
		
		//Aufgabe 3
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
	
		
		System.out.printf( "%.2f\n" ,a);
		System.out.printf( "%.2f\n" ,b);
		System.out.printf( "%.2f\n" ,c);
		System.out.printf( "%.2f\n" ,d);
		System.out.printf( "%.2f\n" ,e);
	
	
	
	
	}

}
