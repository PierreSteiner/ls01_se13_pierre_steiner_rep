
public class Variablenverwenden {

	public static void main(String[] args) {
		/** Variablen.java
		    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
		    @author
		    @version
		*/
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */
			
		int programmdurchläufe ;
		
		/* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			
			programmdurchläufe = 25 ;
			
			System.out.println("programmdurchläufe" + programmdurchläufe);
			
		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */

			char buchstabe;
			
		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/

			buchstabe = 'C' ;
			
			System.out.println("Menüpunkt" + buchstabe);
			
		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */

			long astronomisch;
			
		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/

			astronomisch = 299792458;
			
			System.out.println(astronomisch + "m/s");
			
		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/

			byte AnzahlMitglieder = 20 ;
			
		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

			System.out.println("Anzahl der Mitglieder: " + AnzahlMitglieder);
			
		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/

			long elektrischeladung ;
			
			/*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */

		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/

		  }//main
		}// Variablen
